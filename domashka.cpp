﻿#include <iostream>

using namespace std;

int main() {

	setlocale(0, "Russian");

	const int n = 50;
	int a;
	cout << "1 = нечетные числа, 0 = четные" << endl;
	cin >> a;

	for (int i = 0; i <= n; i++) {
		if (i % 2 == a) {
			cout << i << endl;
		}

	}
}